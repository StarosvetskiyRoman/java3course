from django.test import TestCase
from rest_framework.test import APIClient

from authentication import models


class BaseTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.username = 'TestUser'
        cls.password = 'TestPassword'
        cls.test_user = models.User.objects.create_user(username=cls.username, password=cls.password)

    def setUp(self):
        self.client = APIClient()
        self.client.login(username=self.username, password=self.password)
