from django.db import models


class AdditionalAbstractModelImage(models.Model):

    class Meta:
        abstract = True

    photo = models.ImageField()
