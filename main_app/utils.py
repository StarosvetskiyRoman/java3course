from typing import Callable
import logging

from rest_framework.response import Response

logger = logging.getLogger(__name__)


class ApplyMethodTemplate(object):

    def __init__(self, template_name):
        self.template_name = template_name

    def __call__(self, func: Callable, *args, **kwargs) -> Callable:
        def wrapper(*ars, **krs):
            response = func(*ars, **krs)
            if isinstance(response, Response):
                response.template_name = self.template_name
            return response

        return wrapper
