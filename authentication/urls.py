from django.conf.urls import url
from django.contrib.auth import views as auth_views

from authentication.views import UserRegistrationView, UserProfileView

urlpatterns = [
    url(r'login/', auth_views.LoginView.as_view(template_name='authentication/login.html'), name='login'),
    url(r'logout/', auth_views.LogoutView.as_view(), name='logout'),
    url(r'registration/', UserRegistrationView.as_view(), name='registration'),
    url(r'profile/', UserProfileView.as_view(), name='profile'),
]
