from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):

    is_editor = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)

    def get_user_types_repr(self):
        if not self.is_editor and not self.is_manager:
            return 'Simple user'
        elif self.is_editor and not self.is_manager:
            return 'Editor user'
        elif not self.is_editor and self.is_manager:
            return 'Manager user'
        elif self.is_editor and self.is_manager:
            return 'Editor and manager user'
