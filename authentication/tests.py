from unittest.mock import patch

from django.test import TestCase

from authentication.models import User


@patch('django.db.models', object)
class TestUserModel(TestCase):

    def _get_user(self, is_manager=False, is_editor=False):
        user = User()
        user.is_manager = is_manager
        user.is_editor = is_editor
        return user

    def test_get_user_type_simple_user(self):
        self.assertEqual('Simple user', self._get_user().get_user_types_repr())

    def test_get_user_type_editor_user(self):
        user = self._get_user(is_editor=True)
        self.assertEqual('Editor user', user.get_user_types_repr())

    def test_get_user_type_manager_user(self):
        user = self._get_user(is_manager=True)
        self.assertEqual('Manager user', user.get_user_types_repr())

    def test_get_user_type_manager_and_editor_user(self):
        user = self._get_user(is_manager=True, is_editor=True)
        self.assertEqual('Editor and manager user', user.get_user_types_repr())