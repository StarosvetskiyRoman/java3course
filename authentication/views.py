from django.contrib.auth import login, get_user_model
from django.shortcuts import redirect
from rest_framework.compat import authenticate
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from authentication.forms import UserCreationForm
from main_app.utils import ApplyMethodTemplate
from shop_manager.models import Shop

User = get_user_model()


class UserRegistrationView(APIView):

    def get(self, request):
        form = UserCreationForm()
        data = {
            'form': form,
        }
        return Response(data=data, template_name='authentication/registration.html')

    def post(self, request, *args, **kwargs):
        user_form = UserCreationForm(request.data)
        if user_form.is_valid():
            user_form.save()
            user = authenticate(username=request.data['username'], password=request.data['password2'])
            login(request, user)
            return redirect('/')
        data = {
            'form': user_form,
        }
        return Response(data=data, template_name='authentication/registration.html')


class UserProfileView(GenericAPIView):

    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return self.request.user

    @ApplyMethodTemplate('authentication/profile.html')
    def get(self, request, *args, **kwargs):
        user = self.get_object()
        response_data = {
            'request': request,
            'user': user,
            'user_type': user.get_user_types_repr(),
            'shops': Shop.objects.filter(owner=request.user),
        }
        return Response(data=response_data)
