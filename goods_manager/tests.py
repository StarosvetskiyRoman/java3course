from django.urls import reverse
from rest_framework import status

from goods_manager import models as goods_models
from main_app.tests import BaseTestCase
from shop_manager import models


class BucketViewTestCase(BaseTestCase):

    @staticmethod
    def _create_product(category=goods_models.Good.OTHERS,
                        name='Product name',
                        description='Product description',
                        photo=None):
        return goods_models.Good.objects.create(category=category, name=name, description=description, photo=photo)

    def _create_bucket(self):
        shop = models.Shop.objects.create(name='Shop', description='Shop description', owner=self.test_user)
        offer_1 = models.Offer.objects.create(good=self._create_product(), price=1, shop=shop)
        offer_2 = models.Offer.objects.create(good=self._create_product(), price=2, shop=shop)
        offer_3 = models.Offer.objects.create(good=self._create_product(), price=3, shop=shop)
        models.OwnedGood.objects.create(user=self.test_user, offer=offer_1)
        models.OwnedGood.objects.create(user=self.test_user, offer=offer_2)
        models.OwnedGood.objects.create(user=self.test_user, offer=offer_3)
        return [offer_1, offer_2, offer_3]

    def test_retrieve_bucket_succeed(self):
        response = self.client.get(reverse('catalog:bucket'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_bucket_content_succeed(self):
        expected_data = self._create_bucket()
        response = self.client.get(reverse('catalog:bucket'))
        self.assertEqual(response.data['offers'], expected_data)

    def test_unauthenticated_user_request_failure(self):
        self.client.logout()
        response = self.client.get(reverse('catalog:bucket'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
