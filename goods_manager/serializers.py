from rest_framework.serializers import ModelSerializer

from goods_manager import models
from shop_manager.serializers import OfferSerializer


class GoodSerializer(ModelSerializer):

    offers = OfferSerializer(many=True, read_only=True)

    class Meta:
        model = models.Good
        fields = '__all__'
