from django.db import models

from authentication.models import User


class Good(models.Model):

    OTHERS = 0
    FOOD = 1
    LAPTOPS = 2
    SELL_PHONES = 3
    FOR_HOME = 4
    APPLIANCES = 5

    CATEGORIES = [
        (OTHERS, 'Other goods'),
        (FOOD, 'Food'),
        (LAPTOPS, 'Laptops'),
        (SELL_PHONES, 'Sell phones'),
        (FOR_HOME, 'Goods for home'),
        (APPLIANCES, 'Appliances')
    ]

    category = models.IntegerField(choices=CATEGORIES)
    name = models.CharField(max_length=100)
    description = models.TextField()
    photo = models.ImageField(null=True, blank=True)
