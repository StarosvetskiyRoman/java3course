from typing import Tuple, Iterable

from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.urls import reverse
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from goods_manager.forms import ProductForm
from goods_manager.models import Good
from main_app.utils import ApplyMethodTemplate
from shop_manager.models import Offer, OwnedGood
from shop_manager.permissions import IsManagerPermission


class CatalogViewMixin(object):

    categories = Good._meta.get_field('category').choices

    def get_categories(self):
        return self.categories

    @staticmethod
    def get_border_prices(product: Good) -> Tuple[float, float]:
        offers = Offer.objects.filter(good=product)
        try:
            max_price = max([offer.price for offer in offers])
            min_price = min([offer.price for offer in offers])
            return min_price, max_price
        except ValueError:
            zero_values = (0.0, 0.0)
            return zero_values

    def enhance_products_with_minimum_price(self, products: Iterable):
        for product in products:
            min_price, _ = self.get_border_prices(product)
            if min_price == 0.0:
                min_price = 'No offers'
            setattr(product, 'min_price', min_price)
        return products


class CatalogViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     GenericViewSet,
                     CatalogViewMixin):

    lookup_field = 'goods_pk'
    lookup_value_regex = r'\d+'
    queryset = Good.objects.all()

    def get_object(self) -> Good:
        try:
            return Good.objects.get(pk=self.kwargs[self.lookup_field])
        except ObjectDoesNotExist:
            raise NotFound(detail='Such product does not exist')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        offers = Offer.objects.filter(good=instance)
        min_price, max_price = self.get_border_prices(instance)
        data = {
            'product': instance,
            'offers': offers,
            'min_price': min_price,
            'max_price': max_price,
        }
        return Response(data=data, template_name='goods_manager/product.html')

    def list(self, request, *args, **kwargs):
        products = self.get_queryset()
        products = self.enhance_products_with_minimum_price(products)
        response_data = {
            'products': products,
            'categories': self.categories,
        }
        return Response(data=response_data, template_name='goods_manager/index.html')

    @action(detail=False)
    def category(self, request, *args, **kwargs):
        category_id = request.query_params.get('id', '')
        if not id:
            raise NotFound()
        products = self.get_queryset().filter(category=category_id)
        response_data = {
            'products': products,
            'categories': self.categories,
        }
        return Response(data=response_data, template_name='goods_manager/index.html')


class PutIntoBucketView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        offer_id = self.kwargs['offer_id']
        try:
            offer = Offer.objects.get(pk=offer_id)
        except ObjectDoesNotExist:
            raise NotFound()
        if not OwnedGood.objects.filter(offer=offer, user=request.user).exists():
            owned_good = OwnedGood(offer=offer, user=request.user)
            owned_good.save()
        return redirect('/catalog/bucket/')


class BucketAPIView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        offers = [owned_good.offer for owned_good in OwnedGood.objects.filter(user=request.user)]
        return Response(data={'offers': offers}, template_name='goods_manager/bucket.html')


class CreateProductView(GenericAPIView):

    permission_classes = (IsAuthenticated, IsManagerPermission, )

    @ApplyMethodTemplate('goods_manager/create_product.html')
    def get(self, request, *args, **kwargs):
        form = ProductForm()
        response_data = {
            'form': form,
        }
        response_data.update(**kwargs)
        return Response(data=response_data)

    def post(self, request, *args, **kwargs):
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save()
            return redirect(reverse('catalog:good-detail', kwargs={'goods_pk': product.pk}))
        else:
            kwargs.update({'form': form})
            return self.get(request, *args, **kwargs)


class FilterAPIView(APIView, CatalogViewMixin):

    permission_classes = (AllowAny, )

    @staticmethod
    def _get_shop_names(data):
        return data.keys()

    def get(self, request, *args, **kwargs):
        data = request.query_params.copy()
        shops = self._get_shop_names(data)
        products = Good.objects.filter(offer__shop__name__in=shops)
        products = self.enhance_products_with_minimum_price(products)
        response_data = {
            'products': products,
            'categories': self.get_categories()
        }
        return Response(data=response_data, template_name='goods_manager/index.html')


class SearchAPIView(APIView, CatalogViewMixin):

    permission_classes = (AllowAny, )
    request_line_name = 'request_line'

    def get(self, request, *args, **kwargs):
        line = request.query_params.get(self.request_line_name, None)
        if line is None:
            raise ValidationError()
        products = Good.objects.filter(name__istartswith=line)
        products = self.enhance_products_with_minimum_price(products)
        response_data = {
            'products': products,
            'categories': self.get_categories()
        }
        return Response(data=response_data, template_name='goods_manager/index.html')
