from django.apps import AppConfig


class GoodsManagerConfig(AppConfig):
    name = 'goods_manager'
