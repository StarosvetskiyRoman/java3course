from django.conf.urls import url
from django.urls import include
from rest_framework.routers import SimpleRouter

from goods_manager import views

shop_router = SimpleRouter()
shop_router.register(r'', views.CatalogViewSet)

urlpatterns = [
    url(r'put_into_bucket/(?P<offer_id>\d+)/$', views.PutIntoBucketView.as_view()),
    url(r'bucket/', views.BucketAPIView.as_view(), name='bucket'),
    url(r'create-product/', views.CreateProductView.as_view(), name='create_product'),
    url(r'filter/', views.FilterAPIView.as_view(), name='filter_products'),
    url(r'search/', views.SearchAPIView.as_view(), name='search'),
    url(r'', include(shop_router.urls)),
]
