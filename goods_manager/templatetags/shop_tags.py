from django import template

from shop_manager.models import Shop

register = template.Library()


@register.inclusion_tag('goods_manager/shops_column.html')
def shops_column():
    data = {
        'shops': Shop.objects.all()
    }
    return data
