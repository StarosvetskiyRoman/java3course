from rest_framework.permissions import BasePermission

from shop_manager.models import Shop


class IsManager(BasePermission):

    def has_permission(self, request, view):
        return request.user.is_manager


class IsShopOwner(BasePermission):

    def has_permission(self, request, view):
        shop_id = view.kwargs.get('shop_id', None)
        if not shop_id:
            return False
        if Shop.objects.filter(pk=shop_id, owner=request.user).exists():
            return True
        return False
