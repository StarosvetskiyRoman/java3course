from django.contrib.auth import get_user_model
from django.db import models

from goods_manager.models import Good

UserModel = get_user_model()


class Shop(models.Model):

    name = models.CharField(max_length=100)
    description = models.TextField()
    owner = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    photo = models.ImageField(null=True, blank=True)


class Offer(models.Model):

    good = models.ForeignKey(Good, on_delete=models.CASCADE)
    shop = models.ForeignKey(Shop, on_delete=models.DO_NOTHING)
    price = models.FloatField()


class OwnedGood(models.Model):

    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    user = models.ForeignKey(UserModel, on_delete=models.DO_NOTHING)
