# TODO: apply navigation to profile after success action

from django.shortcuts import redirect
from django.urls import reverse
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView, CreateAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from goods_manager.permissions import IsShopOwner
from main_app.utils import ApplyMethodTemplate
from shop_manager.forms import ShopForm, OfferForm
from shop_manager.models import Shop, Offer
from shop_manager.permissions import ManagerOwnerUpdateOrReadOnly, IsManagerPermission, IsOfferOwner
from shop_manager.serializers import ShopSerializer


class RetrieveUpdateShopManagerView(RetrieveAPIView):

    permission_classes = (ManagerOwnerUpdateOrReadOnly, )
    serializer_class = ShopSerializer
    queryset = Shop.objects.all()
    lookup_field = 'id'
    form = ShopForm

    @ApplyMethodTemplate('shop_manager/index.html')
    def retrieve(self, request, *args, **kwargs):
        shop = self.get_object()
        response_data = {
            'shop': shop,
            'request': request,
        }
        if shop.owner == request.user:
            response_data.update({'form': self.form(instance=shop), **kwargs})
        return Response(data=response_data)

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        form = self.form(request.data)
        # TODO: need to have an ability to reassign shop to another user
        if form.is_valid():
            shop = self.get_object()
            for attr, value in request.data.items():
                setattr(shop, attr, value)
            shop.save()
            return redirect('/')
        else:
            return self.retrieve(request, *args, **kwargs)


class CreateShopManagerView(CreateAPIView):

    permission_classes = (IsManagerPermission, )
    serializer_class = ShopSerializer
    queryset = Shop.objects.all()
    form = ShopForm

    @ApplyMethodTemplate('shop_manager/index.html')
    def get(self, request, *args, **kwargs):
        response_data = {
            'request': request,
            'form': self.form(),
            **kwargs
        }
        return Response(data=response_data)

    def create(self, request, *args, **kwargs):
        form = self.form(request.data)
        if form.is_valid():
            shop = form.save(commit=False)
            shop.owner = request.user
            shop.save()
            return redirect('/')
        else:
            return self.get(request, form=form, *args, **kwargs)


class ListOfferView(GenericAPIView):

    queryset = Offer.objects.all()
    lookup_field = 'shop_id'

    def filter_queryset(self, queryset):
        shop_id = self.kwargs.get(self.lookup_field, None)
        if not shop_id:
            raise NotFound('shop_id is missing in request')
        return queryset.filter(shop_id=shop_id)

    def is_user_shop_owner(self):
        shop_id = self.kwargs.get(self.lookup_field)
        return Shop.objects.filter(pk=shop_id, owner=self.request.user).exists()

    def get_shop(self):
        return Shop.objects.filter(pk=self.kwargs.get(self.lookup_field, None)).first()

    @ApplyMethodTemplate('shop_manager/offers_list.html')
    def get(self, request, *args, **kwargs):
        offers = self.filter_queryset(self.get_queryset())
        is_owner = self.is_user_shop_owner()
        response_data = {
            'request': request,
            'offers': offers,
            'is_owner': is_owner,
            'shop': self.get_shop(),
        }
        return Response(data=response_data)


class CreateOfferView(GenericAPIView):

    permission_classes = (IsAuthenticated, IsShopOwner, )

    @ApplyMethodTemplate('shop_manager/create-offer.html')
    def get(self, request, *args, **kwargs):
        form = OfferForm()
        response_data = {
            'request': request,
            'form': form,
        }
        response_data.update(**kwargs)
        return Response(data=response_data)

    def post(self, request, *args, **kwargs):
        try:
            shop = Shop.objects.get(pk=kwargs['shop_id'])
            form = OfferForm(request.data)
            if form.is_valid():
                offer = form.save(commit=False)
                offer.shop = shop
                offer.save()
                return redirect(reverse('shop:offers-list', kwargs=kwargs))
            else:
                kwargs.update({'form': form})
                return self.get(request, *args, **kwargs)
        except Shop.DoesNotExist:
            raise NotFound('Shop with provided id does not exist')


class OfferDeleteView(GenericAPIView):

    permission_classes = (IsOfferOwner, IsAuthenticated, )
    lookup_field = 'pk'
    queryset = Offer.objects.all()

    def get(self, request, *args, **kwargs):
        offer = self.get_object()
        offer.delete()
        kwargs.pop('pk')
        return redirect(reverse('shop:offers-list', kwargs=kwargs))
