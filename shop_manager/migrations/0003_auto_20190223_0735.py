# Generated by Django 2.1.3 on 2019-02-23 07:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop_manager', '0002_shop_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ownedgood',
            name='good',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='shop_manager.Offer'),
        ),
    ]
