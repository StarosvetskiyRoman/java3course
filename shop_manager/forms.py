from django import forms

from goods_manager.models import Good
from shop_manager import models
from shop_manager.exceptions import IncorrectUrlError
from shop_manager.utils import get_product_pk_from_catalog_url


class ShopForm(forms.ModelForm):

    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))

    class Meta:
        model = models.Shop
        fields = ('name', 'description', )


class OfferForm(forms.ModelForm):

    good = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Put product link here',
        'class': 'form-control',
    }))
    price = forms.NumberInput()

    class Meta:
        model = models.Offer
        fields = ('good', 'price', )
        widgets = {
            'price': forms.NumberInput(attrs={'class': 'form-control'})
        }

    def clean_good(self):
        good_url = self.cleaned_data['good']
        try:
            product_pk = get_product_pk_from_catalog_url(good_url)
            product = Good.objects.get(pk=product_pk)
            return product
        except IncorrectUrlError:
            self.add_error('good', 'Invalid url is provided')
        except Good.DoesNotExist:
            self.add_error('good', 'Such product does not exists')
