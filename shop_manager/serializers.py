from rest_framework import serializers

from authentication.serializers import CommonUserSerializer
from shop_manager import models


class ShopSerializer(serializers.ModelSerializer):
    owner = CommonUserSerializer()

    class Meta:
        model = models.Shop
        fields = ('id', 'name', 'description', 'owner', )


class OfferSerializer(serializers.ModelSerializer):

    shop = ShopSerializer()

    class Meta:
        model = models.Offer
        fields = '__all__'

