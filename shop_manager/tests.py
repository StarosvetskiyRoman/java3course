from unittest.mock import MagicMock

from django.test import TestCase

from shop_manager.exceptions import IncorrectUrlError
from shop_manager.permissions import IsManagerPermission, IsOfferOwner
from shop_manager.utils import clear_from_empty_strings, get_product_pk_from_catalog_url


class TestUtilsClearFormEmptyStrings(TestCase):

    def test_valid_list_succeeds(self):
        provided_list = ['', 'qwe', 'www', '', '', 'dfsd']
        expected_list = ['qwe', 'www', 'dfsd']
        received_list = clear_from_empty_strings(provided_list)
        self.assertListEqual(received_list, expected_list)

    def test_valid_list_with_no_deletions_succeeds(self):
        provided_list = ['qwe', 'www', 'dfsd']
        expected_list = ['qwe', 'www', 'dfsd']
        received_list = clear_from_empty_strings(provided_list)
        self.assertListEqual(received_list, expected_list)

    def test_invalid_input_parameter_type_failure(self):
        provided_parameter = int(0)
        with self.assertRaises(TypeError):
            clear_from_empty_strings(provided_parameter)


class TestUtilsGetProductPkFromCatalogUrl(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.host = 'http://host.com/'

    def _get_url(self, path):
        return self.host + path

    def test_valid_url_succeeds(self):
        product_pk = 123
        provided_url = self._get_url('catalog/{}/'.format(product_pk))
        expected_data = product_pk
        received_data = get_product_pk_from_catalog_url(provided_url)
        self.assertEqual(received_data, expected_data)

    def test_valid_url_with_related_path_succeeds(self):
        product_pk = 123
        provided_url = self._get_url('catalog/{}/related/path/'.format(product_pk))
        expected_data = product_pk
        received_data = get_product_pk_from_catalog_url(provided_url)
        self.assertEqual(received_data, expected_data)

    def test_invalid_product_pk_type_failure(self):
        product_pk = 'product_pk'
        provided_url = self._get_url('catalog/{}/'.format(product_pk))
        with self.assertRaises(IncorrectUrlError):
            get_product_pk_from_catalog_url(provided_url)

    def test_missing_keyword_failure(self):
        product_pk = 123
        provided_url = self._get_url('{}/'.format(product_pk))
        with self.assertRaises(IncorrectUrlError):
            get_product_pk_from_catalog_url(provided_url)

    def test_keyword_invalid_position_failure(self):
        product_pk = 123
        provided_url = self._get_url('{}/catalog/'.format(product_pk))
        with self.assertRaises(IncorrectUrlError):
            get_product_pk_from_catalog_url(provided_url)

    def test_invalid_product_pk_position_failure(self):
        product_pk = 123
        provided_url = self._get_url('catalog/attr/{}/'.format(product_pk))
        with self.assertRaises(IncorrectUrlError):
            get_product_pk_from_catalog_url(provided_url)


class TestManagerOwnerUpdateOrReadOnlyPermission(TestCase):

    def setUp(self):
        self.view_mock = MagicMock()
        self.request_mock = MagicMock()

    def test_request_permitted(self):
        self.request_mock.user.is_authenticated = True
        self.request_mock.user.is_manager = True
        self.assertTrue(IsManagerPermission().has_permission(self.request_mock, self.view_mock))

    def test_request_user_not_manager_declined(self):
        self.request_mock.user.is_authenticated = True
        self.request_mock.user.is_manager = False
        self.assertFalse(IsManagerPermission().has_permission(self.request_mock, self.view_mock))

    def test_request_user_not_authenticated_declined(self):
        self.request_mock.user.is_authenticated = False
        self.request_mock.user.is_manager = True
        self.assertFalse(IsManagerPermission().has_permission(self.request_mock, self.view_mock))

    def test_request_user_not_manager_and_not_authenticated_declined(self):
        self.request_mock.user.is_authenticated = False
        self.request_mock.user.is_manager = False
        self.assertFalse(IsManagerPermission().has_permission(self.request_mock, self.view_mock))


class TestISOfferOwnerPermission(TestCase):
    class StubObject:
        def set_attribute(self, name, value):
            setattr(self, name, value)

    def _get_stub_to_be_permitted(self):
        user = self.StubObject()
        request = self.StubObject()
        request.set_attribute('user', user)
        view = self.StubObject()
        obj = self.StubObject()
        obj.set_attribute('shop', self.StubObject())
        obj.shop.set_attribute('owner', user)
        return request, view, obj

    def _get_stub_to_be_declined(self):
        request = self.StubObject()
        request.set_attribute('user', self.StubObject())
        view = self.StubObject()
        obj = self.StubObject()
        obj.set_attribute('shop', self.StubObject())
        obj.shop.set_attribute('owner', self.StubObject())
        return request, view, obj

    def test_user_owner_permitted(self):
        stub = self._get_stub_to_be_permitted()
        self.assertTrue(IsOfferOwner().has_object_permission(*stub))

    def test_user_not_owner_declined(self):
        stub = self._get_stub_to_be_declined()
        self.assertFalse(IsOfferOwner().has_object_permission(*stub))
