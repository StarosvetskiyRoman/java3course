from django.contrib import admin

from shop_manager.models import Shop, Offer, OwnedGood

admin.site.register(Shop)
admin.site.register(Offer)
admin.site.register(OwnedGood)
