from rest_framework.permissions import BasePermission

only_manager_methods = ('POST',)


class ManagerOwnerUpdateOrReadOnly(BasePermission):

    def has_permission(self, request, view):
        if request.method in only_manager_methods and (request.user.is_anonymous or not request.user.is_manager
                                                       or request.user != view.get_object().owner):
            return False
        return True


class IsManagerPermission(BasePermission):

    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user.is_manager


class IsOfferOwner(BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.shop.owner == request.user
