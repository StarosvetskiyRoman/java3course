from urllib.parse import urlparse

from shop_manager.exceptions import IncorrectUrlError


def clear_from_empty_strings(params: list) -> list:
    empty_strings_indexes = []
    for index, string in enumerate(params):
        if string == '' or string is None:
            empty_strings_indexes.append(index)
    for index in reversed(empty_strings_indexes):
        del params[index]
    return params


def get_product_pk_from_catalog_url(url):
    PK_POSITION = 1
    FLAG_WORDS = (('catalog', 0), )
    path = urlparse(url).path
    params = path.split(sep='/')
    params = clear_from_empty_strings(params)
    for parameter, position in FLAG_WORDS:
        if params[position] != parameter:
            raise IncorrectUrlError
    try:
        product_pk = int(params[PK_POSITION])
        return product_pk
    except ValueError:
        raise IncorrectUrlError
