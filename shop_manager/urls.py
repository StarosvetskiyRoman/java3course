from django.conf.urls import url


from shop_manager import views


urlpatterns = [
    url(r'(?P<shop_id>\d+)/remove-offer/(?P<pk>\d+)/', views.OfferDeleteView.as_view(), name='offer-delete'),
    url(r'(?P<shop_id>\d+)/offers/', views.ListOfferView.as_view(), name='offers-list'),
    url(r'(?P<shop_id>\d+)/create-offer/', views.CreateOfferView.as_view(), name='offer-create'),
    url(r'(?P<id>\d+)/', views.RetrieveUpdateShopManagerView.as_view(), name='retrieve-update-shop'),
    url(r'', views.CreateShopManagerView.as_view(), name='create-shop'),
]
